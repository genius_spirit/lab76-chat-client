import React from "react";
import "./InputPanel.css";
import FormControl from "material-ui/es/Form/FormControl";
import InputLabel from "material-ui/es/Input/InputLabel";
import Button from "material-ui/es/Button/Button";
import Input from "material-ui/es/Input/Input";
import { FormHelperText } from "material-ui";

const InputPanel = ({ change, postMessage, error }) => {
  return (
    <div className="InputPanel">
      <FormControl>
        <InputLabel htmlFor="name-input">Name</InputLabel>
        <Input
          id="name-input"
          className="Input Input-name"
          onChange={event => change(event)}
          name="author"
        />
        {!!error ? (
          <FormHelperText style={{ color: "red" }}>
            {JSON.parse(error).errorName}
          </FormHelperText>
        ) : null}
      </FormControl>
      <FormControl>
        <InputLabel htmlFor="name-input">Message</InputLabel>
        <Input
          id="name-input"
          className="Input Input-message"
          name="message"
          onChange={event => change(event)}
        />
        {!!error ? (
          <FormHelperText style={{ color: "red" }}>
            {JSON.parse(error).errorMessage}
          </FormHelperText>
        ) : null}
      </FormControl>
      <Button variant="raised" size="small" onClick={postMessage}>
        Send
      </Button>
    </div>
  );
};

export default InputPanel;
